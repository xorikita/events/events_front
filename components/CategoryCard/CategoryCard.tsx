import Link from 'next/link'
import s from './CategoryCard.module.css'

export interface Category{
    id: number,
    name: string,
}

export default function CategoryCard({category}:{category: Category}) {
    const backgrounds = [s.bg_1, s.bg_2, s.bg_3, s.bg_4, s.bg_5, s.bg_6]
    const current_bg = backgrounds[Math.floor(Math.random()*backgrounds.length)];
    return(
        <Link
            href={`/category/${category.id}`}
            className={s.card}
        >
            <div className={`${s.card_bg} ${current_bg}`}></div>
            <h3 className={s.title}>{category.name}</h3>
        </Link>
    )
}