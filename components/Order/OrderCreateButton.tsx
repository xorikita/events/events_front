'use client'
import { useState, useContext, useEffect } from 'react'
import { UserContext } from '@/contexts/UserProvider'
import { CreateOrder, OrderData } from '@/api/orderApi'
import { Event } from '../EventCard/EventCard'
import s from'./OrderCreate.module.css'
import Modal from '../Modal/Modal'

export default function OrderCreateButton({event}:{event:Event}){
    const {user, setUser} = useContext(UserContext)
    const [data, setData] = useState<OrderData>({customer_id: Number(user.id), event_id: event.id, date: 'когда-нибудь'})
    const [modal, setModal] = useState(false)
    const [date, setDate] = useState(toDateInputValue(new Date()))

    function toDateInputValue(dateObject: Date){
        const local = new Date(dateObject);
        local.setMinutes(dateObject.getMinutes() - dateObject.getTimezoneOffset());
        return local.toJSON().slice(0,10);
    };

    const changeDate = (value: Date | null) => {
        let now = new Date()
        if(value && value > now){
            setDate(toDateInputValue(value))
        }
        else{
            setDate(toDateInputValue(now))
        }
    }

    useEffect(() => {
        setData({...data, date: date})
    },[date])

    const createOrder = async () => {
        let newOrder = await CreateOrder(data)
        if(newOrder){
            setModal(false)
        }
    }
    return(
        <>
            {user.id? 
                <div>
                    <button className={s.btn} onClick={() => setModal(true)}>Заказать мероприятие</button>
                    <Modal isOpen={modal} toggle={setModal}>
                        <h2>Заказ</h2>
                        <input type='text' value={event.name} readOnly></input>
                        <input type='date' value={date} onChange={(e) => changeDate(e.target.valueAsDate)}></input>
                        <button className={s.btn} onClick={() => createOrder()}>Заказать</button>
                    </Modal>
                </div>:''
            }
        </>
    )
}