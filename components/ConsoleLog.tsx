'use client'
 
import { useState } from 'react'
 
export default function ConsoleLog(data: any) {
    const [log, setLog] = useState(data)
    const logging = () => {
        console.log(log)
    }
    return (
        <button onClick={() => logging()}>Logging</button>
    )
}