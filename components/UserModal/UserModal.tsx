'use client'
import {LoginData} from '@/api/userApi'
import s from './UserModal.module.css'
import { useState, useContext, useEffect } from 'react'
import { UserContext } from '@/contexts/UserProvider'
import { CreateUser, LoginUser } from '@/api/userApi'
import Link from 'next/link'
import Modal from '../Modal/Modal'

export default function UserModal(){
    const [modal, setModal] = useState<boolean>(false)
    const [data, setData] = useState<LoginData>({login: '', password: ''})
    const [mode, setMode] = useState<String>('login')
    const {user, setUser} = useContext(UserContext)
    const [error, setError] = useState<String>('')

    useEffect(() => {
        if(localStorage.getItem('user_id')){
            let saved_id = Number(localStorage.getItem('user_id'))
            let saved_login = localStorage.getItem('user_login')
            setUser({id: saved_id, login: saved_login})
        }
    },[])

    useEffect(() => {
        if(user.id){
            localStorage.setItem('user_id', String(user.id))
            localStorage.setItem('user_login', user.login)
        }
        else{
            localStorage.setItem('user_id', '')
            localStorage.setItem('user_login', '')
        }
        setModal(false)
    }, [user])

    const loginUser = async (e: React.FormEvent<HTMLElement>) => {
        e.preventDefault()
        let newUser = await LoginUser(data)
        setError('')
        newUser.id? setUser(newUser): setError('no user')
        setData({login: '', password: ''})
    }

    const registerUser = async (e: React.FormEvent<HTMLElement>) => {
        e.preventDefault()
        let newUser = await CreateUser(data)
        setError('')
        newUser.id? setUser(newUser): setError('form invalid or login used')
        setData({login: '', password: ''})
    }
    return(
        <div style={{marginLeft: 'auto'}}>
            {
                user.id?
                    <Link className={s.toggler} href={'/my'}>{user.login}</Link>
                    :
                    mode === 'login'?
                    <div>
                        <a className={s.toggler} onClick={() => setModal(true)}>Войти</a>
                        <Modal isOpen={modal} toggle={setModal}>
                            <h2>Вход</h2>
                            <form className={s.form} onSubmit={(e) => loginUser(e)}>
                                <input placeholder='Логин' onInput={(e) => setData({...data, login: e.currentTarget.value})}></input>
                                <input placeholder='Пароль' type='password' onInput={(e) => setData({...data, password: e.currentTarget.value})}></input>
                                <button>Войти</button>
                            </form>
                            {error? <div className={s.error}>{error}</div>: ''}
                            <a className={s.link} onClick={() => setMode('register')}>Регистрация</a>
                        </Modal>
                    </div>
                    :
                    <div>
                        <a className={s.toggler} onClick={() => setModal(true)}>Регистрация</a>
                        <Modal isOpen={modal} toggle={setModal}>
                            <h2>Регистрация</h2>
                            <form className={s.form} onSubmit={(e) => registerUser(e)}>
                                <input placeholder='Логин' onInput={(e) => setData({...data, login: e.currentTarget.value})}></input>
                                <input placeholder='Пароль' type='password' onInput={(e) => setData({...data, password: e.currentTarget.value})}></input>
                                <button>Отправить</button>
                            </form>
                            {error? <div className={s.error}>{error}</div>: ''}
                            <a className={s.link} onClick={() => setMode('login')}>Вход</a>
                        </Modal>
                    </div>
            }
        </div>
    )
}