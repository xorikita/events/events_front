import Link from 'next/link'
import s from './LocationCard.module.css'

export interface Location{
    id: number,
    name: string,
    adress: string,
    price: number
}

export default function LocationCard({location}:{location: Location}){
    return(
        <Link href={`/location/${location.id}`} className={s.nft}>
            <div className={s.main}>
                <h3>{location.name}</h3>
                <p className={s.description}>{location.adress}</p>
                <div className={s.tokenInfo}>
                    <p className={s.description}>
                        От <span className={s.price}>{location.price} &#8381;</span>
                    </p>
                </div>
            </div>
        </Link>
    )
}