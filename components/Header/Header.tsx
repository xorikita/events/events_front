import s from './Header.module.css'

export default function Header({children}: Readonly<{children: React.ReactNode;}>){
    return(
        <header className={s.header}>
            {children}
        </header>
    )
}