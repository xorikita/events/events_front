import s from './OrderLine.module.css'
import {site_url} from '../../api/api'
import Link from 'next/link'
import { Order } from '@/api/orderApi'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrash } from '@fortawesome/free-solid-svg-icons'

export default function OrderLine({order, onDelete}:{order: Order, onDelete: Function}){
    return(
        <article className={s.line}>
            <img className={s.image} src={`${site_url}${order.image}`}></img>
            <div className={s.info}>
                <h3>{order.name}</h3>
                <p>{order.date}</p>
            </div>
            <div className={s.controls}>
                <p>{order.price.toString()} ₽</p>
                <a onClick={() => onDelete(order.id)} className={s.delete}><FontAwesomeIcon icon={faTrash} size='lg'/></a>
            </div>
        </article>
    )
}