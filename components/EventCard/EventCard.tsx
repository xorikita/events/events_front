import s from './EventCard.module.css'
import {site_url} from '../../api/api'
import Link from 'next/link'

export interface Event{
    id: number,
    name: string,
    description: string,
    image: string,
    type_id: number,
    location_id: number,
}

export default function EventCard({event}:{event: Event}){
    return(
        <Link
            href={`/events/${event.id}`}
            key={event.id}
            className={s.card}
            style={{backgroundImage: `url(${site_url}${event.image})`}}
        >
            <div className={s.content}>
                <h3 className={s.title}>{event.name}</h3>
            </div>
        </Link>
    )
}