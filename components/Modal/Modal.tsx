'use client'
import s from './Modal.module.css'
import { ReactNode, SetStateAction, useState } from 'react'

interface ModalType {
    children?: ReactNode;
    isOpen: boolean;
    toggle: React.Dispatch<React.SetStateAction<boolean>>;
}

export default function Modal(props: ModalType){
    return(
        <div className={`${s.modal} ${props.isOpen? s.open: ''}`}>
            <div className={s.modal_body}>
                <button className={s.close} onClick={() => props.toggle(false)}>&#10006;</button>
                {props.children}
            </div>
        </div>
    )
}