import { GetData } from "../../../api/api";
import EventCard, {Event} from "../../../components/EventCard/EventCard";

export default async function Category({params}:{params: any}) {
    const category = await GetData({model: 'Type', id: params.categoryId})
    const data: Array<Event> = [...await GetData({model: 'Event', filters: {type_id: category.id}})]
    return (
        <main>
        <h1>{category.name}</h1>
        <div className='grid-fill'>
            {data?.map(event =>
            (
                <EventCard key={event.id} event={event}></EventCard>
            )
            )}
        </div>
        </main>
    );
}