import { GetData, site_url } from "@/api/api"
import s from './page.module.css'
import Link from "next/link"
import OrderCreateButton from "@/components/Order/OrderCreateButton"

export default async function EventId({params}:{params: any}){
    const event: any = await GetData({model: 'Event', id: params.eventId})
    const category: any = await GetData({model: 'Type', id: event.type_id})
    const location: any = await GetData({model: 'Location', id: event.location_id})
    console.log(event)
    return(
        <main>
            <div className={s.content}>
                <h1>{event.name}</h1>

                <div className={s.image} style={{backgroundImage: `url(${site_url}${event.image})`}}></div>

                <div className={s.description}>
                    {event.description}
                </div>

                <div className={s.footer}>
                    <div className={s.tags}>
                        <Link href={`/category/${category.id}`} className={s.tag}>{category.name}</Link>
                        <Link href={`/location/${location.id}`} className={s.tag}>{location.name}</Link>
                    </div>
                    <span className={s.price}>{event.price} &#8381;</span>
                    <OrderCreateButton event={event}></OrderCreateButton>
                </div>
            </div>
        </main>
    )
}