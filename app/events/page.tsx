import { GetData } from "../../api/api";
import EventCard, {Event} from "../../components/EventCard/EventCard";
import s from './page.module.css';

export default async function Events() {
  const data: Array<Event> = [...await GetData({model: 'Event'})]
  return (
    <main>
      <h1>Мероприятия</h1>
      <div className='grid-fill'>
        {data?.map(event =>
          (
            <EventCard key={event.id} event={event}></EventCard>
          )
        )}
      </div>
    </main>
  );
}