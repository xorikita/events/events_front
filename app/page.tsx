import { GetData } from "../api/api";
import CategoryCard, {Category} from "../components/CategoryCard/CategoryCard";
import LocationCard, {Location} from "../components/LocationCard/LocationCard";
import s from './page.module.css';

export default async function Home() {
  const categories: Array<Category> = [...await GetData({model: 'Type'})]
  const locations: Array<Location> = [...await GetData({model: 'Location'})]
  return (
    <main>
      <h1 className="main_title">Категории</h1>
      <h2>Праздники</h2>
      <div className='grid-fit'>
        {categories?.map(category =>
          (
            <CategoryCard key={category.id} category={category}></CategoryCard>
          )
        )}
      </div>
      <h2>Места проведения</h2>
      <div className='grid-fit'>
        {locations?.map(location =>
          (
            <LocationCard key={location.id} location={location}></LocationCard>
          )
        )}
      </div>
    </main>
  );
}
