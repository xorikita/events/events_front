"use client"
import { UserContext } from "@/contexts/UserProvider";
import { useContext, useEffect, useState } from "react";
import { useRouter } from "next/navigation";
import { GetOrders, Order, DeleteOrder } from "@/api/orderApi";
import OrderLine from "@/components/OrderLine/OrderLine";

export default function My() {
  const router = useRouter()
  const {user, setUser} = useContext(UserContext)
  const [orders, setOrders] = useState<Array<Order>>([])

  useEffect(() => {
    if(!user.id){
      router.push('/')
    }
  },[user])

  useEffect(() => {
    async function getOrders() {
      const data = [...await GetOrders(Number(user.id))]
      console.log(data)
      console.log(user.id)
      setOrders([...data])
    }
    if(user.id) getOrders()
  }, [])

  const deleteHandler = async (id: Number) => {
    await DeleteOrder(id)
    setOrders([...orders.filter(item => item.id !== id)])
  }
  return (
    <main>
      <h1 className="main_title">{user.login}</h1>
      <div className='ordersTable'>
        {orders?.map(order => {
          console.log(order)
          return(
            <OrderLine order={order} onDelete={deleteHandler}/>
          )
        })}
      </div>
      <a onClick={() => setUser({id: null, login: ''})}>Выйти</a>
    </main>
  );
}
