import { GetData } from "../../../api/api";
import EventCard, {Event} from "../../../components/EventCard/EventCard";

export default async function Location({params}:{params: any}) {
    const location = await GetData({model: 'Location', id: params.locationId})
    const data: Array<Event> = [...await GetData({model: 'Event', filters: {location_id: location.id}})]
    return (
        <main>
        <h1>{location.name}</h1>
        <div className='grid-fill'>
            {data?.map(event =>
            (
                <EventCard key={event.id} event={event}></EventCard>
            )
            )}
        </div>
        </main>
    );
}