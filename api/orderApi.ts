'use server'
import { api_url } from "./api";
import { revalidateTag } from "next/cache"

export interface OrderData{
    customer_id: Number,
    event_id: Number,
    date: String
}

export interface Order{
    date: String,
    id: Number,
    image: String,
    name: String,
    price: Number,
}

const createOrderData = (data: OrderData) => {
    return { method: 'POST', headers: { "Content-Type": "application/json", }, body: JSON.stringify(data), next: { tags: [`customer: ${String(data.customer_id)}`] } }
}

const createIdData = (orderId: Number) => {
    return { method: 'POST', headers: { "Content-Type": "application/json", }, body: JSON.stringify({id: orderId}), next: { tags: [`order: ${String(orderId)}`] } }
}

export async function CreateOrder(orderData:OrderData) {
    const res = await fetch(`${api_url}/create_order/`, createOrderData(orderData));
    const data = res.status === 200 ? await res.json() : []
    console.log(res)
    revalidateTag(`customer: ${String(orderData.customer_id)}`)
    return data;
}


export async function DeleteOrder(orderId:Number) {
    const res = await fetch(`${api_url}/delete_order/`, createIdData(orderId));
    const data = res.status === 200 ? await res.json() : []
    console.log(data)
    revalidateTag(`order: ${String(orderId)}`)
    return data;
}

export async function GetOrders(userId:Number) {
    const res = await fetch(`${api_url}/get_orders/`, createIdData(userId));
    const data = res.status === 200 ? await res.json() : []
    console.log(data)
    revalidateTag(`order: ${String(userId)}`)
    return data;
}