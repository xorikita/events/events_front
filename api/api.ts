import { revalidateTag } from "next/cache"
export const api_url = 'http://admin.waifu-xorikita.ru/events'
export const site_url = 'http://admin.waifu-xorikita.ru'
const createPostData = (data: any) => {
    return { method: 'POST', headers: { "Content-Type": "application/json", }, body: JSON.stringify(data), next: { tags: [data.model] } }
}
export async function GetData(postData: { model: string, id?: number, filters?: object}) {
    const res = await fetch(`${api_url}/get_data/`, createPostData(postData));
    const data = res.status === 200 ? await res.json() : []
    revalidateTag(postData.model)
    return data;
}