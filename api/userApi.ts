'use server'
import { api_url } from "./api";
import { revalidateTag } from "next/cache"

export interface LoginData{
    login: string,
    password: string,
}

const createUserData = (data: LoginData) => {
    return { method: 'POST', headers: { "Content-Type": "application/json", }, body: JSON.stringify(data), next: { tags: [data.login] } }
}

export async function CreateUser(loginData:LoginData) {
    const res = await fetch(`${api_url}/register/`, createUserData(loginData));
    const data = res.status === 200 ? await res.json() : []
    revalidateTag(data.login)
    return data;
}


export async function LoginUser(loginData:LoginData) {
    const res = await fetch(`${api_url}/login/`, createUserData(loginData));
    const data = res.status === 200 ? await res.json() : []
    revalidateTag(data.login)
    return data;
}