'use client';
import React, { createContext, useState } from 'react';

export interface User{
    id: number | null,
    login: string,
}

type StateContextType = {
  user: User;
  setUser: any;
};

export const UserContext = createContext<StateContextType>(
  null as unknown as StateContextType,
);

type ContextProviderProps = {
  children: React.ReactNode;
};

export const UserProvider = ({ children }: ContextProviderProps) => {
  const [user, setUser] = useState({id: null, login: ''});


  return (
    <UserContext.Provider value={{user, setUser}}>{children}</UserContext.Provider>
  );
};